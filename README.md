# How to use this

This will read the contents of an `images/` directory (from stdin) and
process a template for a given `MACHINE`:
```
dir2bundle <MACHINE>
```

In the directory of the build, where all artifacts are stored:

```
ls | ./dir2bundle juno
```
or, for example, for a remote S3 bucket containing those artifacts:
```
aws s3 ls s3://storage.lkft.org/rootfs/oe-sumo/20200205/dragonboard-410c/ | cut -c32- | ./dir2bundle dragonboard-410c
```

The output will be a `bundle.json` file that can be consumed by
[ci-scripts](https://gitlab.com/Linaro/lkft/ci-scripts)'s `gen-variables.sh`.
